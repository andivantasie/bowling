<?php

interface FrameInterface
{
    public function getRolls(): array;
    public function isBonus(): bool;
    public function isStrike(): bool;
    public function isSpare(): bool;
    public function addScore(int $score): void;
    public function getScore(): int;
}
