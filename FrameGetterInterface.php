<?php

interface FrameGetterInterface
{
    public function getFrames(): array;
}