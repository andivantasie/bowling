<?php

require 'FrameInterface.php';

class Frame implements FrameInterface
{
    private $rolls;
    private $score;
    private $isBonus;
    private $isStrike = false;
    private $isSpare = false;

    public function __construct(array $rolls, bool $isBonus = false)
    {
        $this->rolls = $rolls;
        $this->score = $this->rolls[0] + $this->rolls[1];
        $this->isBonus = $isBonus;
        if ($this->rolls[0] === 10) {
            $this->isStrike = true;
        }
        if ($this->rolls[0] !== 10 && $this->rolls[0] + $this->rolls[1] === 10) {
            $this->isSpare = true;
        }
    }

    public function getRolls(): array
    {
        return $this->rolls;
    }

    public function isBonus(): bool
    {
        return $this->isBonus;
    }

    public function isStrike(): bool
    {
        return $this->isStrike;
    }

    public function isSpare(): bool
    {
        return $this->isSpare;
    }

    public function addScore(int $score): void
    {
        $this->score += $score;
    }

    public function getScore(): int
    {
        return $this->score;
    }
}
