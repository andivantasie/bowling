# Let's get ready to bowl

Inspiriert durch einen Programmier-Vorschlag aus dem [Coding Dojo][1] ist eine in PHP geschriebene Bowling-Simulation entstanden.

## Spiel starten

`./game`

## To-dos

* Export der Ergebnisse
* Umsetzung für mehrere User

[1]: https://codingdojo.org/kata/Bowling/
