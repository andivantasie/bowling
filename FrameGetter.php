<?php

require 'Frame.php';
require 'FrameGetterInterface.php';

class FrameGetter implements FrameGetterInterface
{
    private $numberOfFrames;

    public function __construct(int $numberOfFrames = 10)
    {
        $this->numberOfFrames = $numberOfFrames;
    }

    /**
     * @param int $roll
     * @return int
     * @throws Exception
     */
    private function getPinsRoll(int $roll): int
    {
        $pinsRoll = readline('Roll ' . $roll . ': ');
        if ($pinsRoll >= 0 && $pinsRoll <= 10) {
            return $pinsRoll;
        } else {
            throw new Exception('Only 10 pins available.');
        }
    }

    /**
     * @return Frame
     * @throws Exception
     */
    private function getFrame(): Frame
    {
        $rollsFrame = [0, 0];
        for ($i = 0; $i < sizeof($rollsFrame); $i++) {
            try {
                $rollsFrame[$i] = $this->getPinsRoll($i + 1);
                if ($rollsFrame[$i] === 10) {
                    break;
                }
            } catch (Exception $e) {
                die('Error: ' . $e->getMessage() . "\n");
            } catch (TypeError $e) {
                die("Error: Number required.\n");
            }
        }
        if (array_sum($rollsFrame) <= 10) {
            return new Frame($rollsFrame);
        } else {
            throw new Exception('Only 10 pins available.');
        }
    }

    private function getBonusFrame(): Frame
    {
        $rollsFrame = [0, 0, 0];
        for ($i = 0; $i < sizeof($rollsFrame) - 1; $i++) {
            try {
                $rollsFrame[$i] = $this->getPinsRoll($i + 1);
            } catch (Exception $e) {
                die('Error: ' . $e->getMessage() . "\n");
            } catch (TypeError $e) {
                die("Error: Number required.\n");
            }
        }
        // strike or spare?
        if ($rollsFrame[0] === 10 ||
            ($rollsFrame[0] !== 10 && $rollsFrame[0] + $rollsFrame[1] === 10)
        ) {
            try {
                $rollsFrame[$i] = $this->getPinsRoll($i + 1);
            } catch (Exception $e) {
                die('Error: ' . $e->getMessage() . "\n");
            } catch (TypeError $e) {
                die("Error: Number required.\n");
            }
        }
        return new Frame($rollsFrame, true);
    }

    public function getFrames(): array
    {
        echo "=== LET'S GET READY TO BOWL! ===\n\n";
        $frames = [];
        for ($i = 0; $i < $this->numberOfFrames - 1; $i++) {
            echo '=== Frame ' . ($i + 1) . " ===\n";
            try {
                $frames[] = $this->getFrame();
            } catch (Exception $e) {
                die('Error: ' . $e->getMessage() . "\n");
            }
        }
        echo '=== Frame ' . ($i + 1) . " ===\n";
        $frames[] = $this->getBonusFrame();
        return $frames;
    }
}