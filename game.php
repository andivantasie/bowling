#!/usr/bin/env php

<?php

require 'FrameGetter.php';
require 'ScoreCalculator.php';
require 'ResultPrinter.php';

$frameGetter = new FrameGetter();
$scoreCalculator = new ScoreCalculator($frameGetter);
$resultPrinter = new ResultPrinter($scoreCalculator);
$resultPrinter->displayResults();
