<?php

require 'ScoreCalculatorInterface.php';

class ScoreCalculator implements ScoreCalculatorInterface
{
    private $frameGetter;

    public function __construct(FrameGetterInterface $frameGetter)
    {
        $this->frameGetter = $frameGetter;
    }

    public function getScoredFrames(): array
    {
        $frames = $this->frameGetter->getFrames();
        for ($i = 0; $i < sizeof($frames); $i++) {
            if ($frames[$i]->isBonus()) {
                if ($frames[$i]->isStrike() || $frames[$i]->isSpare()) {
                    $frames[$i]->addScore($frames[$i]->getRolls()[2]);
                }
            } else {
                if ($frames[$i]->isStrike()) {
                    if ($frames[$i+1]->isSpare()) {
                        $frames[$i]->addScore($frames[$i+1]->getScore());
                    }
                    if ($frames[$i+1]->isStrike()) {
                        if ($frames[$i+1]->isBonus()) {
                            $frames[$i]->addScore($frames[$i+1]->getRolls()[0] + $frames[$i+1]->getRolls()[1]);
                        } else {
                            $frames[$i]->addScore($frames[$i+1]->getRolls()[0] + $frames[$i+2]->getRolls()[0]);
                        }
                    }
                }
                if ($frames[$i]->isSpare()) {
                    $frames[$i]->addScore($frames[$i+1]->getRolls()[0]);
                }
            }
        }
        return $frames;
    }
}