<?php

require 'ResultPrinterInterface.php';

class ResultPrinter implements ResultPrinterInterface
{
    private $scoreCalculator;

    public function __construct(ScoreCalculatorInterface $scoreCalculator)
    {
        $this->scoreCalculator = $scoreCalculator;
    }

    public function displayResults(): void
    {
        $frames = $this->scoreCalculator->getScoredFrames();
        $totalScore = 0;
        echo "\n=== Results ===\n\n";
        for ($i = 0; $i < sizeof($frames); $i++) {
            $totalScore += $frames[$i]->getScore();
            echo '=== Frame ' . ($i + 1) . " ===";
            if ($frames[$i]->isStrike()) {
                echo ' <<< STRIKE >>>';
            }
            if ($frames[$i]->isSpare()) {
                echo ' <<< SPARE >>>';
            }
            echo "\n";
            echo 'Roll 1: ' . $frames[$i]->getRolls()[0] . "\n";
            if ($frames[$i]->isBonus()) {
                echo 'Roll 2: ' . $frames[$i]->getRolls()[1] . "\n";
                if ($frames[$i]->isStrike() || $frames[$i]->isSpare()) {
                    echo 'Roll 3: ' . $frames[$i]->getRolls()[2] . "\n";
                }
            } else {
                if (!$frames[$i]->isStrike()) {
                    echo 'Roll 2: ' . $frames[$i]->getRolls()[1] . "\n";
                }
            }
            echo 'Score: ' . $frames[$i]->getScore() . "\n";
            echo 'Total Score: ' . $totalScore . "\n";
        }
    }
}