<?php

interface ResultPrinterInterface
{
    public function displayResults(): void;
}