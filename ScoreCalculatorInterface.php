<?php

interface ScoreCalculatorInterface
{
    public function getScoredFrames(): array;
}